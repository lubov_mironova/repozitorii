package ru.mla.calculater;

public class MathematicInt {

    /**
     *Нахождение суммы
     *
     * @param a Первое слагаемое
     * @param b Второе слагаемое
     * @return Сумму
     */
    static int addition(int a, int b){
        return a + b;
    }

    /**
     * Нахождение разности
     *
     * @param a Уменьшаемое
     * @param b Вычитаемое
     * @return Разность
     */
    static int subtraction(int a, int b){
        return a - b;
    }

    /**
     * Нахождение произведения
     *
     * @param a Первый множитель
     * @param b Второй множитель
     * @return Произведение
     */
    static int multiplication(int a, int b){
        return a * b;
    }

    /**
     * Нахождение частного
     *
     * @param a Делимое
     * @param b Делитель
     * @return Частное
     */
    static int division(int a, int b){
        return a / b;
    }

    /**
     * Нахождение остатка
     *
     * @param a Делимое
     * @param b Делитель
     * @return Остаток
     */
    static int divisionEnd(int a, int b){
        return a % b;
    }

    /**
     * Возведение в степень
     *
     * @param a Основание степени
     * @param b Показатель степени
     * @return Возведенное в степень число
     */
    static int power(int a, int b){
        int c = 1;
        for (int i = 0; i < b; i++) {
            c = c * a;
        }
        return c;
    }
}