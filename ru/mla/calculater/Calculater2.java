package ru.mla.calculater;
/**
 * Класс для проведения различных арифммитических действий с двумя числами
 *
 * @author Mironova L.
 */

import java.util.Scanner;

public class Calculater2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number1, number2;


        System.out.println("Введите выражение для расчета\nВведите два числа и выбраннй вами знак операции из списка: +, -, *, /, %, ^;\nВведите их через пробел");
        String line = "";
        line = scanner.nextLine();
        String[] numb = line.split(" ");
        number1 = Integer.valueOf(numb[0]);
        number2 = Integer.valueOf(numb[2]);

        switch (numb[1]){
            case "+":
                int result = MathematicInt.addition(number1, number2);
                System.out.println("Сумма чисел " + number1 + " и " + number2 + " равна " + result);
                break;
            case "-":
                result = MathematicInt.subtraction(number1, number2);
                System.out.println("Разность чисел " + number1 +" и " + number2 +" равна " + result);
                break;
            case "*":
                result = MathematicInt.multiplication(number1, number2);
                System.out.println("Произведение чисел " + number1 +" и " + number2 +" равно " + result);
                break;
            case "/":
                result = MathematicInt.division(number1, number2);
                System.out.println("Деление чисел " + number1 +" и " + number2 +" равно " + result);
                break;
            case "%":
                result = MathematicInt.divisionEnd(number1, number2);
                System.out.println("Остаток равен " + result);
                break;
            case "^":
                if (number2 == 0){
                    result = 1;
                    System.out.println(number1 + " в степени " + number2 + " равно 1");
                    return;
                }
                if (number2 < 0) {
                    result = (1 / MathematicInt.power(number1, number2));
                    System.out.println(number1 + " в степени " + number2 + " равно " + result);
                    return;
                }
                if (number2 > 0) {
                    result = MathematicInt.power(number1, number2);
                    System.out.println(number1 + " в степени " + number2 + " равно " + result);
                }
        }
    }
}
