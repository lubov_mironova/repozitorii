package ru.mla.calculater;

import java.util.Scanner;

public class Calculater {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int number1, number2;

        System.out.printf("Выберите одной из действий:%n1) Сложение%n2) Вычитание%n3) Умножение%n4) Деление%n5) Выделение остатка%n6) Возведение в степень%nВведите номер действия: ");
        int num = scanner.nextInt();
        System.out.println("Введите числа для рассчета: ");
        number1 = scanner.nextInt();
        number2 = scanner.nextInt();

        if (num != 0 || num < 7) {
            if (num == 1){
                int result = MathematicInt.addition(number1, number2);
                System.out.println("Сумма чисел " + number1 + " и " + number2 + " равна " + result);
            }
            if (num == 2){
                int result = MathematicInt.subtraction(number1, number2);
                System.out.println("Разность чисел " + number1 +" и " + number2 +" равна " + result);
            }
            if (num == 3){
                int result = MathematicInt.multiplication(number1, number2);
                System.out.println("Произведение чисел " + number1 +" и " + number2 +" равно " + result);
            }
            if (num == 1){
                int result = MathematicInt.division(number1, number2);
                System.out.println("Деление чисел " + number1 +" и " + number2 +" равно " + result);
            }
            if (num == 5){
                int result = MathematicInt.divisionEnd(number1, number2);
                System.out.println("Остаток равен " + result);
            }
            if (num == 6){
                if (number2 < 0){
                    System.out.println("Введен неверный показатель степени");
                    return;
                }
                int result = MathematicInt.power(number1, number2);
                System.out.println(number1 +" в степени "+ number2 +" равно "+ result);
            } else {
                System.out.println("Действие выбрано неверно");
            }
        }
    }
}
