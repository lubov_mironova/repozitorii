package ru.mla.temperature;

import java.util.Scanner;

/**
 * Класс для рассчета максимальной и минимальной температуры
 *
 * @author Mironova L.
 */
public class Temperature {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOfDays;
        System.out.print("Введите количество дней:");
        numberOfDays = scanner.nextInt();
        double temperature[] = new double[numberOfDays];

        fillTemperature(temperature);
        System.out.printf("%5.1f", averageTemperature(temperature));
        System.out.printf("%n Минимальная температура %d числа равна = %.1f", minDayTemperature(temperature), minTemperature(temperature));
        System.out.printf("%n Максимальная температура %d числа равна = %.1f %n", maxDayTemperature(temperature), maxTemperature(temperature));
    }

    /**
     * Метод для рандома температур
     *
     * @param temperature массив температур
     */
    private static void fillTemperature(double temperature[]) {
        for (int i = 0; i < temperature.length; i++) {
            temperature[i] = -10 + (Math.random() * 30);
            System.out.printf("%1.1f ", temperature[i]);
        }
    }

    /**
     * Метод для нахождения средней температуры
     *
     * @param temperature массив температур
     * @return среднюю температуру
     */
    private static double averageTemperature(double temperature[]) {
        double sum = 0.0;
        for (double temperatureOfDay : temperature) {
            sum = sum + temperatureOfDay;
        }
        System.out.printf("%6.1f", sum);
        return sum / temperature.length;
    }

    /**
     * Метод для нахождения минимальной температуры
     *
     * @param temperature массив температур
     * @return минимальную температуру
     */
    private static double minTemperature(double temperature[]) {
        double min = temperature[0];
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] < min) {
                min = temperature[i];
                int day = i;
            }
        }
        return min;
    }

    /**
     * Метод для нахождения дней с минимальной температурой
     *
     * @param temperature массив температур
     * @return дни с минимальной температурой
     */
    private static int minDayTemperature(double temperature[]) {
        double min = temperature[0];
        int day = 1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] < min) {
                min = temperature[i];
                day= i + 1;
            }
        }
        return day;
    }

    /**
     * Метод для нахождения максимальной температуры
     *
     * @param temperature массив температур
     * @return максимальную температуру
     */
    private static double maxTemperature(double temperature[]) {
        double max = temperature[0];
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] > max) {
                max = temperature[i];
            }
        }
        return max;
    }

    /**
     * Метод для нахождения дней с максимальной температурой
     *
     * @param temperature массив температур
     * @return дни с максимальной температурой
     */
    private static int maxDayTemperature(double temperature[]) {
        double max = temperature[0];
        int day = 1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] > max) {
                max = temperature[i];
                day=i+1;
            }
        }
        return day;
    }
}

