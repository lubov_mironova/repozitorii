package ru.mla.other;
/**
 * Класс для проверки строки на палиндромность
 *
 * @author Mironova l.
 */

import java.util.Scanner;

public class Palindrome {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите фразу для проверки: ");
        StringBuffer line = new StringBuffer();
        line = line.append(scanner.nextLine());

        String firstLine = line.toString();
        String secondLine = reverseBufToStr(line);

        String workFirstLine = lineWithoutAll(firstLine);
        String workSecondLine = lineWithoutAll(secondLine);

        boolean retVal = workFirstLine.equalsIgnoreCase(workSecondLine);
        if (retVal == true) {
            System.out.println("Строка является палиндромом");
        } else {
            System.out.println("Строка не является палиндромом");
        }
    }

    /**
     * Метод возвращает введенную с консоли строку в обратной последовательности символов
     *
     * @param line введенная с консоли строка
     * @return введенную с консоли строку в обратной последовательности символов
     */
    private static String reverseBufToStr(StringBuffer line) {
        line = line.reverse();
        String strSecLine = line.toString();
        return strSecLine;
    }

    /**
     * Метод убирает знаки препинания и пробелы из введенной с консоли строки
     *
     * @param line введенная с консоли строка
     * @return введенную с консоли строку без знаков препинания и пробелов
     */
    private static String lineWithoutAll(String line){
        String firstLine = line.replaceAll("[\\s()?:!.,;{}\"-]","");
        return firstLine ;
    }
}